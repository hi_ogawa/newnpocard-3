<?php		// conf_add01.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
	$today_year = date("Y");
	$_SESSION['conf_add'] = hash("sha512", $today_year);

	if (!isset($_SESSION['auth_admin'])||($_SESSION['auth_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
?>

<html><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xlmns="http://www.w3.org/1993/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../mem_reg/member_registration.css">
	<title>NPO TRI</title>
</head>
<body>
<div id="main">
<h1>カンファランス登録画面 [1/4]</h1>

<table>
<form action="conf_add02.php" method="post">
	
    <tr><td>カンファランス名 : </td>
    <td><input type="text" id="conf_jname" name="conf_jname" size=40 maxlength=64 class="in"></td></tr>
    <tr><td>カンファランス名(半角アルファベット) : </td>
    <td><input type="text" id="conf_ename" name="conf_ename" size=40 maxlength=64 class="in"></td></tr> 
    <tr><td>開始日(YYYY-MM-DD) : </td>
    <td><input type="text" id="begin" name="begin" size=10 maxlength=10 class="in"></td></tr>     
    <tr><td>終了日(YYYY-MM-DD) : </td>
    <td><input type="text" id="end" name="end" size=10 maxlength=10 class="in"></td></tr> 
    <tr><td>場所 : </td>
    <td><input type="text" id="jplace" name="jplace" size=40 maxlength=64 class="in"></td></tr> 
    <tr><td>Venue : </td>
    <td><input type="text" id="eplace" name="eplace" size=40 maxlength=64 class="in"></td></tr> 
    <tr><td>人数 : </td>
    <td><input type="text" id="man_size" name="man_size" size=5 maxlength=5 class="in"></td></tr> 

<tr><td colspan="2" align="center"><input type="submit" value="- 確認 -"></td></tr>
</form>
</table>
</div>

</body>
</html>