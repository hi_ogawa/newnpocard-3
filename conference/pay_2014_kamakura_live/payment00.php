<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/
//if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {

if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])||!isset($_POST['job_kind'])||!is_numeric($_POST['job_kind'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
		$job_kind = $_POST['job_kind'];
	}
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/


/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/

/*
	if (!isset($_POST['conf_link_tbl_id'])||!is_numeric($_POST['conf_link_tbl_id'])) {
	if (!isset($_POST['conf_link_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_link_tbl_id = $_POST['conf_link_tbl_id'];
	}	
*/
/*********************************************************
	2014/07/02変更（終わり）
*********************************************************/



	if (isset($_SESSION['error_msg'])) {
		echo "<script type='text/javascript'>";
		echo "alert('".$_SESSION['error_msg']."');";
		echo "</script>";
	}
	
	$_SESSION['can_go_payment01'] = true;
?>    	
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">


<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
 
 <!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/jumbotron/jumbotron.css">


</head>

<body>
      <h1 class="alert-success">&nbsp;&nbsp;Welcome to Payment System</h1>
 
<div class="jumbotron"  style="background-color:rgba(170,156,3,1.00); padding-top:25px; padding-bottom:5px;">
      <div class="container">
        <p  class="white">TRIも、経カテーテル的大動脈弁植え込み術 (TAVI/TAVR)もこれまでの標準的治療法よりも低侵襲に患者さんを治療しよう、という根本概念においては通じるものがあります。2014年の鎌倉ライブデモンストレーションでは最終日をTREND (Transcatheter Endocardiovascular Intervention Conference)と共同開催されます。<br><br>
        This year, the last day of KAMAKUR LIVE will be jointly held with TREND (Transcatheter Endocardiovascular Intervention Conference).</p>
 <p><a class="btn btn-danger btn-lg" role="button" id="program2014">Current Program for KAMAKURA LIVE 2014 &raquo;</a></p>
  </div>
    </div>
<!--
/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/
-->
   	<form name="payment00" method="post" id="form_to_payment01dr">
	        <input type="hidden" name="dr_tbl_id" value="<?=$dr_tbl_id?>" />
	        <input type="hidden" name="conf_tbl_id" value="<?=$conf_tbl_id?>" />
	        <input type="hidden" name="job_kind" value="<?=$job_kind?>" />
        </form>
	<script type="text/javascript">
		function next2Page(url)
		{
			document.forms["payment00"].action=url;
			document.forms["payment00"].submit();
		}
	</script>
<!--
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/
-->
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">

<!--
/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/
-->
<?php
	//医師用
	if($job_kind==1 )
	{
?>
        <div class="col-md-12">
        <table class="table table-striped table-bordered border-thick">
        <tr><td colspan="5">
          <h2 class="bg-black">医師用 鎌倉ライブ2014 前登録支払い </h2>
          <p class="font-large">事前登録および事前カード決済は 特定非営利活動法人ティー・アール・アイ国際ネットワーク年会費 3,000円を含み、大変<span class="font-red">お得</span>になっています。<br>
          <span class="font-red font-small">注:カード決済の場合いかなる理由でも払い戻し不可となります</span></p></td></tr>
          <tr><td>支払い種別</td><td>金額</td><td>19日(金)</td><td>20日(土)</td><td>21日(日) TREND</td></tr>
          <tr>
            <td class="left">前登録支払い(カード決済)</td>
            <td class="large-red">15,000円</td><td class="green">参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td><td class="money">20,000円</td><td class="green">支払日・参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td><td class="money">20,000円</td><td class="black">参加不可</td><td class="green">支払日・参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">10,000円</td>
          <td class="black">参加不可</td><td class="black">参加不可</td><td class="green">支払日・参加可能</td></tr>

<!--
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="payment01dr.php" role="button" id="payment_dr">事前カード決済する(医師用) &raquo;</a></p></td></tr>
-->
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="#" onclick="next2Page('payment01dr.php')" role="button" id="payment_dr">事前カード決済する(医師用) &raquo;</a></p></td></tr>
          </table>   
          </div>
<?php
	}//if($job_kind==1) 医師用
?>
<!--
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/
-->

<!--
/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/
-->
<?php
	//コメディカル用　ただし officialsをはぶく
	if($job_kind==2 || $job_kind==3 || $job_kind==4 || $job_kind==5 || $job_kind==6 || $job_kind==7 || $job_kind==8)
	{
?>
          <p>&nbsp;</p>
        <div class="col-md-12">
        <table class="table table-striped table-bordered border-thick">
        <tr><td colspan="5">
          <h2 class="bg-black">コメディカル用 鎌倉ライブ2014 前登録支払い </h2>
          <p class="font-large">事前登録および事前カード決済は 特定非営利活動法人ティー・アール・アイ国際ネットワーク年会費 1,000円を含み、大変<span class="font-red">お得</span>になっています</p></td></tr>
          <tr><td>支払い種別</td><td>金額</td><td>19日(金)</td><td>20日(土)</td><td>21日(日) TREND</td></tr>
          <tr><td class="left">全登録支払い(カード決済)</td>
          <td class="large-red">2,000円</td>
          <td class="green">参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">3,000円</td>
          <td class="green">支払日・参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">3,000円</td>
          <td class="black">参加不可</td><td class="green">支払日・参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">3,000円</td>
          <td class="black">参加不可</td><td class="black">参加不可</td><td class="green">支払日・参加可能</td></tr>
<!--
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="payment01come.php" role="button" id="payment_come">事前カード決済する(コメディカル用) &raquo;</a></p></td></tr>          
-->
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="#" onclick="next2Page('payment01come.php')" role="button" id="payment_come">事前カード決済する(コメディカル用) &raquo;</a></p></td></tr>          
          </table>   
      </div>
<?php
	}//if($job_kind==2 || $job_kind==3 || $job_kind==4 || $job_kind==5 || $job_kind==6 || $job_kind==7 || $job_kind==8 || $job_kind==9)コメディカル用
?>
<!--
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/
-->
      

<!--
/*********************************************************
	2014/09/03変更（はじめ）
*********************************************************/
-->
<?php
	//officials用
	if($job_kind==9)
	{
?>
          <p>&nbsp;</p>
        <div class="col-md-12">
        <table class="table table-striped table-bordered border-thick">
        <tr><td colspan="5">
          <h2 class="bg-black">Officials/Authorities用 鎌倉ライブ2014 前登録 </h2>
          <p class="font-large">このページは Officials/Authorities (具体的にはPMDA: 独立行政法人　医薬品医療機器総合機構あるいは厚生労働省職員あるいは報道関係者用の前登録ページです</p></td></tr>
          <tr><td>支払い種別</td><td>金額</td><td>19日(金)</td><td>20日(土)</td><td>21日(日) TREND</td></tr>
          <tr><td class="left">全登録支払い</td>
          <td class="large-red">免除</td>
          <td class="green">参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">免除</td>
          <td class="green">支払日・参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">免除</td>
          <td class="black">参加不可</td><td class="green">支払日・参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">免除</td>
          <td class="black">参加不可</td><td class="black">参加不可</td><td class="green">支払日・参加可能</td></tr>
<!--
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="payment01come.php" role="button" id="payment_come">事前カード決済する(コメディカル用) &raquo;</a></p></td></tr>          
-->
          <tr><td colspan="5">
          <h2 class="bg-black alert-danger">下のボタンをクリックして事前登録を事務局にご連絡下さい </h2>
          <br><p><a class="btn btn-success" href="#" role="button" onclick="next2Page('payment_official.php')" id="payment_official">事務局に連絡し参加登録する &raquo;</a></p></td></tr>          
          </table>   
      </div>
<?php
	}//if($job_kind==2 || $job_kind==3 || $job_kind==4 || $job_kind==5 || $job_kind==6 || $job_kind==7 || $job_kind==8 || $job_kind==9)コメディカル用
?>
<!--
/*********************************************************
	2014/09/03変更（終わり）
*********************************************************/
-->

<!--
/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/
-->
<?php
	//企業用
	if($job_kind==10)
	{
?>
          <p>&nbsp;</p>
        <div class="col-md-12">
          <table class="table table-striped table-bordered border-thick">
          <tr><td colspan="5">        
          <h2 class="bg-black">企業の方用 鎌倉ライブ2014 前登録支払い </h2>
          <p class="font-large">事前登録および事前カード決済は 、大変<span class="font-red">お得</span>になっています</p></td></tr>
          <tr><td>支払い種別</td><td>金額</td><td>19日(金)</td><td>20日(土)</td><td>21日(日) TREND</td></tr>
          <tr><td class="left">全登録支払い(カード決済)</td>
          <td class="large-red">20,000円</td>
          <td class="green">参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">25,000円</td>
          <td class="green">支払日・参加可能</td><td class="green">参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">25,000円</td>
          <td class="black">参加不可</td><td class="green">支払日・参加可能</td><td class="green">参加可能</td></tr>
          <tr><td class="left">当日支払い(現金のみ)</td>
          <td class="money">20,000円</td>
          <td class="black">参加不可</td><td class="black">参加不可</td><td class="green">支払日・参加可能</td></tr>
<!--
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="payment01company.php" role="button" id="payment_company">事前カード決済する(企業の方用) &raquo;</a></p></td></tr>
-->
          <tr><td colspan="5"><br><p><a class="btn btn-success" href="#" onclick="next2Page('payment01company.php')" role="button" id="payment_company">事前カード決済する(企業の方用) &raquo;</a></p></td></tr>
          </table>          
      </div>      
<?php
	}//if($job_kind==10) 企業用
?>
<!--
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/
-->
      </div>
      <hr>

      <footer>
        <p>&copy; 2013 - 2014 by NPO TRI International Network</p>
      </footer>
    </div> <!-- /container -->
    
    
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="live2014.js"></script>

</body>
</html>
