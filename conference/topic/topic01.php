<?php
// topic01.php
// Inititally, this program was planned to be the entry screen for topic submit.
// However, most of this program was moved to index.php.
// Now, this program is no more used.
//
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();
/*
	$today = date('Y-m-d');
	echo $today; echo "<br />";
	echo strtotime($today);exit;
*/
	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (auth_dr()) {
		//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}
		$stmt1 = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `begin` > now();");
		$stmt1->execute();
		$rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
		
		$stmt2 = $pdo->prepare("SELECT * FROM `conf_link_tbl` WHERE `conf_tbl_id` = :conf_tbl_id AND `dr_tbl_id` = :dr_tbl_id;");
	} else {
		$_SESSION = array();
		header('Location: ../../index.php');
	}
	
	if (isset($_SESSION['topic_title'])) $_SESSION['topic_title'] = "";
	if (isset($_SESSION['topic_abstract'])) $_SESSION['topic_abstract'] = "";
	if (isset($_SESSION['role_kind'])) $_SESSION['role_kind'] = "";

?>		
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="maniac.css"/>
<title>NPO TRI International Network</title>
</head>

<body>
<div id="main">
<h1>カンファランスの選択</h1>
<h2>カンファランスに演題登録するためには、最初に参加登録する必要があります</h2>
<h3><a href="../../index.php">参加登録する</a></h3>
<table>
<tr><th>Choose</th><th>Conference Name</th><th>Begin Date</th><th>Place</th></tr>
<?php
	foreach($rows1 as $value) {
		$stmt2->bindValue(":conf_tbl_id", $value['conf_tbl_id']);
		$stmt2->bindValue(":dr_tbl_id", $_SESSION['dr_tbl_id']);
		$stmt2->execute();
		$num = count($stmt2->fetchAll());
		if ($num>0) {
?>
	<tr><td><form action="topic02.php" method="post"><input type="submit" value="Choose" />
    <input type="hidden" name="conf_tbl_id" value="<?= $value['conf_tbl_id'] ?>" />
    <input type="hidden" name="dr_tbl_id" value="<?= $_SESSION['dr_tbl_id'] ?>" />
    </form></td>
<?php
		} else {
?>
	<tr><td><br /></td>
<?php
		}
?>
    <td><?= $value['conf_ename'] ?></td><td><?= $value['begin'] ?></td><td><?= $value['eplace'] ?></td></tr>
<?php
	}
?>
</div>
</body>
</html>
