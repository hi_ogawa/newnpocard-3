<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='white'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}
/*********************************************************
	2014/07/02変更（はじめ）
*********************************************************/
/*
	if (!isset($_POST['conf_link_tbl_id'])||!is_numeric($_POST['conf_link_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_link_tbl_id = $_POST['conf_link_tbl_id'];
	}	
*/
/*********************************************************
	2014/07/02変更（終わり）
*********************************************************/
	if (auth_dr()) {
		//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}

		$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `id` = :conf_tbl_id;");
		$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
		$stmt->execute();
		$rows = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt1 = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
		$stmt1->bindValue(":dr_tbl_id", $dr_tbl_id);
		$stmt1->execute();
		$rows1 = $stmt1->fetch(PDO::FETCH_ASSOC);
		$_SESSION['sirname'] = $rows1['sirname'];
		$_SESSION['firstname'] = $rows1['firstname'];

	}

?>		
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<script src="../../javascript/jquery-1.10.2.js"></script>
<script src="../../javascript/jquery-corner.js"></script>
<script src="topic.js"></script>
<link rel="stylesheet" type="text/css" href="topic_css.css"/>
<title>NPO TRI International Network</title>
</head>

<body>

<div id="main">
<h1>Contributing Your Presentation</h1>
<h2>This is your presentation during <?= $rows['conf_ename'] ?>.<br />
これは <?= $rows['conf_jname'] ?>におけるあなたの演題登録です</h2>

<form action="topic03.php" method="post" name="frm">
	<input type="hidden" name="dr_tbl_id" value="<?= _Q($dr_tbl_id) ?>">
    <input type="hidden" name="conf_tbl_id" value="<?= _Q($conf_tbl_id) ?>">

    <table>	
    <tr><td class="title">Your Name</td>
    <td class="title"><?= _Q($_SESSION['sirname']).' '._Q($_SESSION['firstname']);?></td></tr>     
	<tr><td class="title">Your Role</td>
    <td><select id="role_kind" name="role_kind" id="role_kind">
    <?php
		foreach($role_kinds as $role_kind => $role_name) {
    		echo "<option value=$role_kind ";
			if (isset($_SESSION['role_kind'])&&($role_kind == $_SESSION['role_kind'])) echo "selected";
			if ($role_kind>2) echo "disabled";
			echo ">$role_name</option>";
		}
	?>
        </select>
    </td></tr>
	<tr><td class="title">Presentation Title :<br /><span class="alert">Up to 128 characters<br />日本語では64文字まで</span></td>
	<td><textarea name="topic_title" rows="3" cols="60"><?php if (isset($_SESSION['topic_title'])) echo _Q($_SESSION['topic_title']); ?></textarea></td></tr>
<!--
/*********************************************************
	2014/07/17変更（はじめ）
*********************************************************/
<tr><td class="title">Presentation Abstract :<br /><span class="alert">Up to 2,000 characters<br />日本語では1,000文字ま;</span></td>
-->
<tr><td class="title">Presentation Abstract :<br /><span class="alert">Up to 2,000 characters<br />日本語では1,000文字まで</span></td>
<!--
/*********************************************************
	2014/07/17変更（終わり）
*********************************************************/
-->
<td><textarea name="topic_abstract" rows="18" cols="60"><?php if (isset($_SESSION['topic_abstract'])) echo _Q($_SESSION['topic_abstract']); ?></textarea></td></tr></table> 
    </form>
<br /><br />
<div id="responce"><button id="confirm" class="confirm">Submit (投稿する)</button>　　　　　　　　<button id="cancel">Cancel (戻る)</button></div>
<br /><br />
</div>
</body>
</html>
