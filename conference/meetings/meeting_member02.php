<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");
	charSetUTF8();	
	session_start();

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}
	

	if (!auth_dr()) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `id` = :conf_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$_SESSION['conf_jname'] = $row['conf_jname'];
    $_SESSION['conf_ename'] = $row['conf_ename']; 
    $_SESSION['begin'] = $row['begin'];
    $_SESSION['end'] = $row['end'];
    $_SESSION['jplace'] = $row['jplace'];
    $_SESSION['eplace'] = $row['eplace'];
    $_SESSION['man_size'] = $row['man_size'];

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$_SESSION['dr_name'] = $row['dr_name'];
	$_SESSION['sirname'] = $row['sirname'];
	$_SESSION['firstname'] = $row['firstname'];
	$_SESSION['is_male'] = $row['is_male'];
	$_SESSION['birth_year'] = $row['birth_year'];
	$_SESSION['hp_name'] = $row['hp_name'];
	$_SESSION['job_kind'] = $row['job_kind'];
	$_SESSION['email'] = $row['email'];

	$stmt = $pdo->prepare("SELECT * FROM  `conf_link_tbl` WHERE `conf_tbl_id` = :conf_tbl_id AND `dr_tbl_id` = :dr_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	if ($stmt->rowCount() > 0) $error = 'あなたは既に'._Q($_SESSION['conf_jname']).'へ登録されています<br />';
	else $error = '';	
?>
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">

<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
    
    <!-- Custom styles for this template -->
	<style type="text/css">
		td {text-align:left; padding-left:3px;}
		.container {width: 90%; background-color:skyblue; height:90%;}
		.center {margin-left:auto; margin-right:auto;text-align:center;}
		h1 {color:white;}
	</style>
</head>

<body>
<div class="space"><p></p></div>
<div class="container">
<h1 class="center">Participation in <?=  _Q($_SESSION['conf_ename']) ?></h1>
<br>
<?php
	if ($error == '') {
		$stmt = $pdo->prepare("SELECT * FROM `conf_link_tbl` WHERE `conf_tbl_id` = :conf_tbl_id;");
		$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
		$stmt->execute();
		$registered = $stmt->rowCount();
		$registable = $_SESSION['man_size'] - $registered;
?>
<table class="table-striped center">

    <tr><td >Conference Name : </td>
    <td ><?= _Q($_SESSION['conf_ename']) ?></td></tr> 
    <tr><td>Stariing on : </td>
    <td><?= _Q($_SESSION['begin']) ?></td></tr>     
    <tr><td >Ending on : </td>
    <td ><?= _Q($_SESSION['end']) ?></td></tr> 
    <tr><td>場所 : </td>
    <td><?= _Q($_SESSION['jplace']) ?></td></tr> 
    <tr><td>Venue : </td>
    <td><?= _Q($_SESSION['eplace']) ?></td></tr> 
    <tr><td >Remaining capacity : </td>
    <td><?= $registable ?></td></tr> 

	<td>氏名 : </td><td class="content"><?= _Q($_SESSION['dr_name']) ?></td></tr>
    <tr><td>Alphabetical Name :  </td><td class="content"><?= _Q($_SESSION['sirname']).'  '._Q($_SESSION['firstname']) ?></td></tr>     
 	<tr><td>Sex : </td><td>
    <?php
		if ($_SESSION['is_male'] == 1) echo "Male";
		else echo "Female";
	?>
    </td></tr>
	<tr><td>Birth Year : </td><td class="content">
		<?php
			list($year, $mon, $day) = explode('-', $_SESSION['birth_year']);
			echo $year." YEAR";
		?>
	</td></tr>
	<tr><td>Affiliation : </td>
    <td><?= _Q($_SESSION['hp_name']) ?></td></tr>
	<tr><td>Job : </td><td><?= $job_kinds[$_SESSION['job_kind']] ?></td></tr>
	<tr><td>Email address : </td><td class="content"><?= _Q($_SESSION['email']) ?></td></tr>
<tr>
<form action="meeting_member03.php" method="post" id="goto03">
	<input type="hidden" name="dr_tbl_id" value="<?= $dr_tbl_id ?>" >
    <input type="hidden" name="conf_tbl_id" value="<?= $conf_tbl_id ?>">
	<td colspan="2" align="center"><br><br><button class="btn btn-large btn-primary" type="button" id="ok">- I want to register to <?= _Q($_SESSION['conf_jname']) ?> with the above information -</button></td>
    </form>
</tr>

</table>
<br><br>
<?php
	} else {
		echo "<h1>$error</h1>";
		echo "<h1><br /><br /><br /></h1>";
		echo "<a href = '../../auth/logout.php'><h1>logout</h1></a>";
		$_SESSION = array();
		session_destroy();
		exit();
	}
?>
</div>
</div>
</div>
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	jQuery(function() {
		 $("#ok").click(function() {
			 $("#goto03").submit();
		 });
	});
	</script>
</body>
</html>