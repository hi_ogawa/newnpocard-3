﻿<?php
	//index.php
	//**************************************************************
	//	CSVファイル取り込み情報画面
	//	
	//**************************************************************

	require_once(dirname(__FILE__).'/../../../utilities/config.php');
	require_once(dirname(__FILE__).'/../../../utilities/lib.php');	
	mb_language("Japanese");
	mb_internal_encoding("UTF-8");
	mb_http_output('UTF-8');

	session_start();

	if (!isset($_SESSION['auth_tool_admin'])||($_SESSION['auth_tool_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
/*
	if (!isset($_POST['index_key'])||($_POST['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../../auth/tool_login.php');
	}
*/
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/index.css"/>
<script src="../../../javascript/jquery-1.10.2.js"></script>
<title>NPO Registration</title>
</head>

<body>
<div id="title">
GMOテストサイトシステム
</div>

		    	<form enctype="multipart/form-data" action="register_order_from_gmo.php" method="post" >
			        <input type="hidden" name="MAX_FILE_SIZSE" value="30000" />
			        <input type="file" name="importFile" id="importFile" value="ファイル指定" />
			        <input type="submit" id="submit" value="取込" />
		        </form>

<a target="_blank" href="https://kt01.mul-pay.jp/kanri/shop/tshop00015048/login">GMOサイトへ</a>
</body>
</html>
