﻿<?php
	require_once('utilities/config.php');
	require_once('utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);
	session_start();
	$_SESSION['last_time'] = time();	// session timeoutのための変数
//	$_SESSION = array();
	$_SESSION['index_key'] = hash("sha512", $magic_code);
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
 <script src="javascript/jquery-1.10.2.js"></script>
<script src="javascript/jquery-corner.js"></script>
<script src="javascript/index.js"></script>
<title>NPO Registration</title>
</head>

<body>
<div id="title">
NPOティー・アール・アイ国際ネットワーク登録システム
<div id="eng_title">Registration System of NPO TRI International Network</div>
</div>
<div class="center">
<?php
	if (auth_dr()) {
?>
<p class="welcome">Welcome Mr/Ms <?= _Q($_SESSION['sirname']); ?>　　</p>
<?php
	} else {
?>
<p class="welcome">Login is needed! (ログインが必要です)</p>
<?php
	}

	if (!auth_dr()) {
		header("Location: index.php");
	} else {	// これ以降は auth_dr()の場合
//接続
 		try {
    	// MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
		// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
		} catch(PDOException $e){
    		die($e->getMessage());
		}

/*********************************************************
	2014/08/21変更（はじめ）
*********************************************************/

	$sqlStr="SELECT FEE.id as item_id,FEE.type as item_type,FEE.name as item_name,FEE.fee,FEE.begin as begin, FEE.ct_begin as ct_begin, ";
	$sqlStr=$sqlStr."FEE.conf_ename as conf_ename,ODR.is_ok as is_paid, FEE.id as conf_tbl_id,FEE.job_kind as job_kind ";
	$sqlStr=$sqlStr."FROM  ";
//注文関連情報
	$sqlStr=$sqlStr."( ";
	$sqlStr=$sqlStr."SELECT ot.dr_tbl_id, omt.item_tbl_id,  ot.is_ok ";
	$sqlStr=$sqlStr."FROM order_tbl ot INNER JOIN order_meisai_tbl omt ON ot.id = omt.order_tbl_id ";
	$sqlStr=$sqlStr."WHERE (((ot.dr_tbl_id)=:dr_tbl_id) AND ((omt.is_deleted)=0) AND ((ot.is_deleted)=0)) ";
	$sqlStr=$sqlStr.") ODR ";
	$sqlStr=$sqlStr." RIGHT JOIN "; 
//支払アイテムリスト
	$sqlStr=$sqlStr."( ";
	$sqlStr=$sqlStr."SELECT it.id as item_tbl_id,ct.id AS id, ift.fee, it.type as type, it.name as name, dt.id AS dr_tbl_id, it.begin as begin, it.end as end, ct.conf_jname as conf_jname, ct.conf_ename as conf_ename,dt.job_kind  as job_kind,ct.begin as ct_begin ";
	$sqlStr=$sqlStr."FROM  ((dr_tbl dt RIGHT JOIN (item_tbl it RIGHT JOIN item_fee_tbl ift ON it.id = ift.item_tbl_id) ON dt.job_kind = ift.job_kinds_tbl_id) LEFT JOIN conf_tbl ct ON ift.item_tbl_id = ct.item_tbl_id)  ";
	$sqlStr=$sqlStr."WHERE (((dt.id)=:dr_tbl_id)) ";
	$sqlStr=$sqlStr.")FEE ";
	$sqlStr=$sqlStr."ON ODR.item_tbl_id = FEE.item_tbl_id ";
	$sqlStr=$sqlStr."  WHERE  FEE.type=1 AND FEE.begin <=now() AND FEE.end >=now() ";

//echo $_SESSION['dr_tbl_id'];
//echo $sqlStr;exit;
/*
		$stmt = $pdo->prepare("SELECT * FROM `conf_link_tbls` INNER JOIN `conf_tbls` ON `conf_link_tbls`.`conf_tbl_id` = ".
			"`conf_tbls`.`id` WHERE `conf_link_tbls`.`dr_tbl_id` = :dr_tbl_id AND DATEDIFF(`begin` , CURRENT_DATE( )) > :abstract_deadline;");
*/
		$stmt = $pdo->prepare($sqlStr);
/*********************************************************
	2014/08/21変更（終わり）
*********************************************************/

		$stmt->bindValue(":dr_tbl_id", $_SESSION['dr_tbl_id']);
		$stmt->bindValue(":abstract_deadline", $abstract_deadline);
		$stmt->execute();


		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($stmt->rowCount()>0) {	// rowCount() conf_link_tbl
?>
 
    
<!--           ここから学会事前登録払込			------->  
  
	<h3 class="index_table">You can buy discounted ticket in the following meeting(s).<br>
    以下の会で、割引事前登録が可能です</h3>
    <table class="registered_table">
    <tr><th class="registered_table">Date</th></th><th class="registered_table">Conference Name</th><th class="registered_table">Prepayment (事前払込)</th>
    </tr>
<?php
			foreach($rows as $value) {
				if (isset($_SESSION['role_kind'])) unset($_SESSION['role_kind']);
				if (isset($_SESSION['topic_title'])) unset($_SESSION['topic_title']);
				if (isset($_SESSION['topic_abstract'])) unset($_SESSION['topic_abstract']);
?>

	<tr>           
    <td class="registered_table"><?= _Q($value['ct_begin']) ?></td>						
    <td class="registered_table"><?= _Q($value['conf_ename']) ?></td>
<?php
	if (!$value['is_paid']) {
?>
    <td class="registered_table">
    	<form action="conference/pay_2014_kamakura_live/payment00.php" method="post">
        	<input type="submit" value="Discount Pre-paymen (割引事前払込)"  class="submit_index">

<!--
/*********************************************************
	2014/08/21追加（はじめ）
*********************************************************/
-->
<!--
            <input type="hidden" name="conf_link_tbl_id" value="<?= _Q($value['id']) ?>">
-->
           	<input type="hidden" name="conf_tbl_id" value="<?= _Q($value['conf_tbl_id']) ?>">
            <input type="hidden" name="dr_tbl_id" value="<?= _Q($_SESSION['dr_tbl_id']) ?>">


           	<input type="hidden" name="job_kind" value="<?= _Q($value['job_kind']) ?>">
<!--
/*********************************************************
	2014/07/21追加（終わり）
*********************************************************/
-->
		</form>
     </td>
<?php
	} else {		// is_paid
		echo "<td class='registered_table'>Prepaid (払込完了)</td>";
	}
?>  
	</tr>   
<?php
			}	// foreach
?>
    </table>
        
<?php

		}	// rowCount() conf_link_tbl
?>

    <br /><br />

	<button class="logout" id="logout">Logout (ログアウト)</button>
<?php
	}	// auth_dr()の場合
?>
<div id="lower">
	<form  action="auth/auth_login.php" method="post">
    <input type="submit" id="submit" value="  " />
    </form>
</div>
</div>
</body>
</html>
