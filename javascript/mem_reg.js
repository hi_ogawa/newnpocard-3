// javascript Document

function init() {
	var delets = document.getElementsByClassName("pw");
	for (var i=0; i<2; i++) {
		deletes[i].value = "";
	}
}

$(function() {
	var dr_name = '姓名(全角16文字まで)';
	$('#dr_name')
		.addClass('watermark')
		.val(dr_name)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === dr_name) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(dr_name);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === dr_name) {
				$(this).val('');
			}
		});
});

$(function() {
	var sir_alpha = '姓 (alphabet)';
	$('#sirname')
		.addClass('watermark')
		.val(sir_alpha)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === sir_alpha) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(sir_alpha);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === sir_alpha) {
				$(this).val('');
			}
		});
});
$(function() {
	var first = '名 (alphabet)';
	$('#firstname')
		.addClass('watermark')
		.val(first)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === first) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(first);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === first) {
				$(this).val('');
			}
		});
});
$(function() {
	var hp = '施設名 (全角16文字まで)';
	$('#hp')
		.addClass('watermark')
		.val(hp)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === hp) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(hp);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === hp) {
				$(this).val('');
			}
		});
});
$(function() {
	var hint = 'あなたの回答 (暗号化されます)';
	$('#hint')
		.addClass('watermark')
		.val(hint)
		.focus(function() {
			$(this).removeClass('watermark');
			if ($(this).val() === hint) {
				$(this).val('');
			}
		})
		.blur(function() {
			if ($(this).val() === '') {
				$(this).val(hint);
				$(this).addClass('watermark');
			}
		})
		.submit(function() {
			if ($(this).val() === hint) {
				$(this).val('');
			}
		});
});
