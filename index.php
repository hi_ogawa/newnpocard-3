<?php
	require_once('utilities/config.php');
	require_once('utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);
	session_start();
	$_SESSION['last_time'] = time();	// session timeoutのための変数
//	$_SESSION = array();
	$_SESSION['index_key'] = hash("sha512", $magic_code);
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="css/index.css"/>
 <script src="javascript/jquery-1.10.2.js"></script>
<script src="javascript/jquery-corner.js"></script>
<script src="javascript/index.js"></script>
<title>NPO Registration</title>
</head>

<body>
<div id="title">
NPOティー・アール・アイ国際ネットワーク登録システム
<div id="eng_title">Registration System of NPO TRI International Network</div>
</div>
<div class="center">
<?php
	if (auth_dr()) {
?>
<p class="welcome">Welcome Mr/Ms <?= _Q($_SESSION['sirname']); ?>　　</p>
<?php
	} else {
?>
<p class="welcome">Login is needed! (ログインが必要です)</p>
<?php
	}

	if (!auth_dr()) {
?>
	<button class="toplines" id="login">Login (ログイン)</button>
    <br /><br />
    <button class="newmember" id="new">If your attempt to login is the first time, click this button and register your personal information first please.<br/>
     (初めてログインされる方はこのボタンを押してあなたの名前などを登録して下さい)</button>
    <div id="explanation">
    In order to register yourself to the meetings which are held by NPO TRI International Conference, you are required to register your information first.<br />
    <br />特定非営利活動法人ティー・アール・アイ国際ネットワークが開催する会合に登録されるためには、最初にあなたの名前などの情報を登録して頂く必要があります
    </div>
    
<?php
	} else {	// これ以降は auth_dr()の場合
	//  認証に成功したので振込か演題登録かに振り分ける
?>
	<button id="payment">参加事前登録</button><h1></h1><h1></h1>
    <button id="abstract">演題登録</button>


<!---------------------------------- ここまで削除 --------------------------->
    <br><br>

	<button class="logout" id="logout">Logout (ログアウト)</button>
<?php
	}	// auth_dr()の場合
?>
<div id="lower">
	<form  action="auth/auth_login.php" method="post">
    <input type="submit" id="submit" value="  " />
    </form>
</div>
</div>
</body>
</html>
